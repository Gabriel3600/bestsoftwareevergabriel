package com.cruxconsultores;

import org.omg.CORBA.Context;
import org.omg.CORBA.ContextList;
import org.omg.CORBA.DomainManager;
import org.omg.CORBA.ExceptionList;
import org.omg.CORBA.NVList;
import org.omg.CORBA.NamedValue;
import org.omg.CORBA.Object;
import org.omg.CORBA.Policy;
import org.omg.CORBA.Request;
import org.omg.CORBA.SetOverrideType;
import org.omg.CosNaming.BindingIteratorHolder;
import org.omg.CosNaming.BindingListHolder;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtPackage.InvalidAddress;
import org.omg.CosNaming.NamingContextPackage.AlreadyBound;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.NotEmpty;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import com.cruxconsultores.interfaces.NamingImpl;

public class Ga implements NamingImpl, NamingContextExt {

	@Override
	public Object resolve_str(String arg0) throws NotFound, CannotProceed,
			InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NameComponent[] to_name(String arg0) throws InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String to_string(NameComponent[] arg0) throws InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String to_url(String arg0, String arg1) throws InvalidAddress,
			InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bind(NameComponent[] arg0, Object arg1) throws NotFound,
			CannotProceed, InvalidName, AlreadyBound {
		// TODO Auto-generated method stub

	}

	@Override
	public void bind_context(NameComponent[] arg0, NamingContext arg1)
			throws NotFound, CannotProceed, InvalidName, AlreadyBound {
		// TODO Auto-generated method stub

	}

	@Override
	public NamingContext bind_new_context(NameComponent[] arg0)
			throws NotFound, AlreadyBound, CannotProceed, InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void destroy() throws NotEmpty {
		// TODO Auto-generated method stub

	}

	@Override
	public void list(int arg0, BindingListHolder arg1,
			BindingIteratorHolder arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public NamingContext new_context() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void rebind(NameComponent[] arg0, Object arg1) throws NotFound,
			CannotProceed, InvalidName {
		// TODO Auto-generated method stub

	}

	@Override
	public void rebind_context(NameComponent[] arg0, NamingContext arg1)
			throws NotFound, CannotProceed, InvalidName {
		// TODO Auto-generated method stub

	}

	@Override
	public Object resolve(NameComponent[] arg0) throws NotFound, CannotProceed,
			InvalidName {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unbind(NameComponent[] arg0) throws NotFound, CannotProceed,
			InvalidName {
		// TODO Auto-generated method stub

	}

	@Override
	public Request _create_request(Context arg0, String arg1, NVList arg2,
			NamedValue arg3) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Request _create_request(Context arg0, String arg1, NVList arg2,
			NamedValue arg3, ExceptionList arg4, ContextList arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object _duplicate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DomainManager[] _get_domain_managers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object _get_interface_def() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Policy _get_policy(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int _hash(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean _is_a(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean _is_equivalent(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean _non_existent() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void _release() {
		// TODO Auto-generated method stub

	}

	@Override
	public Request _request(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object _set_policy_override(Policy[] arg0, SetOverrideType arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void showYourName() {
		// TODO Auto-generated method stub

	}

}
